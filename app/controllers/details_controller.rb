class DetailsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  
  def create
    
    @course = Course.find(params[:course_id])
    @details = Detail.all
    
    exist = false;

    for detail in @details
      if detail.registered == current_user.email
        exist = true;
      end
    end

    if exist == false
      @detail = @course.details.create(detail_params)
      redirect_to course_path(@course)
    else
      flash[:alert] = "you are already enrolled in this class"
      redirect_to course_path(@course)
    end
  end
 
  def destroy
    @course = Course.find(params[:course_id])
    @detail = @course.comments.find(params[:id])
    @details.destroy
    redirect_to course_path(@course)
  end
 
  private
    def detail_params
      params.require(:detail).permit(:registered)
    end
end
