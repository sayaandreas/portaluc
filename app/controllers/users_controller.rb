class UsersController < ApplicationController
  
  def index
  	@users = User.all
  end

  def show
    @user = User.find(params[:id])
  end

  def destroy
    @user = User.find(params[:id])
    if @user.email == current_user.email

      redirect_to users_path
      flash[:alert] = "You can't delete your own account"
    else
      if @user.destroy
 	    redirect_to users_path
 	  end
  	end
  end

  def update
  	
  end

end
