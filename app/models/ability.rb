class Ability
  include CanCan::Ability

  def initialize(user)
    
    if user.role == "Admin"
      can :manage, :all

    elsif user.role == "Dosen"
      can :manage, Course
      can :read, Detail

    elsif user.role == "Mahasiswa"
      can :read, Course
      can :manage, Detail
    end

  end
end
