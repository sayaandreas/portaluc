Rails.application.routes.draw do
  devise_for :users, :path_prefix => 'd'
  resources :users, :only =>[:show, :destroy, :create, :update]

  root 'home#index'

  resources :courses do
  	resources :details
  end

  match '/users/:id',     to: 'users#show', via: 'get'
  match '/users',     to: 'users#index', via: 'get'
  match '/users/:id',     to: 'users#destroy', via: 'delete'
end
